# Rise of Civilizations - Server
A server backend application for a 4X multiplayer game.

# Public endpoints
* `/about/appInfo`: Basic information about the application
  * application name
  * version
  
# Docker image
* **How to build:** `docker build .`  
* **Default exposed port:** 8080