# Rise of Civilizations Server Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## 0.1

* REST interface
* General release and deploy configurations
* Add badges to group level
* Move badges to group level
* Add SAST
* Create templates
* Adding license to production code
* Containerization
* GitLab CI/CD configuration"