## Release description
Official manual release to version <version number>.

## Requirements
- [ ] **VRR** (Verification Readiness Review) is ready and successful
- [ ] **Version number updated**
- [ ] **SNAPSHOT removed**

/label ~release
/assign @zsoltgyorgyszabo
