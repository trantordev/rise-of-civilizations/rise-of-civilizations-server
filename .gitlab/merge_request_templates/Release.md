<!-- Use a descriptive title maximum 72 character long. -->

# Release <version>

**1. Make sure that you've checked the boxes below before you submit MR:**

- [ ] I have read [Contribution guidelines](https://gitlab.com/trantordev/rise-of-civilizations-server/-/blob/master/CONTRIBUTING.md)
- [ ] I have run the code locally and there is no error.
- [ ] I updated the [README.md](https://gitlab.com/trantordev/rise-of-civilizations-server/-/blob/master/README.md) with the relevant information.
- [ ] I added license to every new file with command `gradle licenseFormatMain` and there are no license errors and incompatibilities.
- [ ] no conflict with **production** branch.
- [ ] I updated the version number

**2. Related issue for this MR (optional)**  


/assign me
/label ~release
